import React, { useState } from "react";

import {
	RESPONSE_TYPE_RESULT,
	RESPONSE_TYPE_QUESTION,
	URL_COMMUNITY,
} from "@rainbird/sdk";
import { Rainbird, useResponse } from "@rainbird/sdk-react";

const App = () => (
	<Rainbird
		baseURL={URL_COMMUNITY}
		apiKey="37582f71-b9b1-4451-b9e6-c130c7fd2412"
		kmID="2fd1be28-b38d-4fa3-8b9d-b0976821912c"
		subject="John"
		relationship="speaks"
		object=""
		onError={(e) => <div>Error: {e.message}</div>}
		onLoad={() => <div>Loading...</div>}
	>
		{({ type, data }) => {
			switch (type) {
				case RESPONSE_TYPE_QUESTION:
					const { question } = data;
					const { prompt, subject, relationship, object } = question;
					return (
						<Question
							prompt={prompt}
							subject={subject}
							relationship={relationship}
							object={object}
						/>
					);
				case RESPONSE_TYPE_RESULT:
					return data.map((r) => <Result result={r} />);
				default:
					return <div>Unknown response type: {type}</div>;
			}
		}}
	</Rainbird>
);

const Question = ({ prompt, subject, relationship, object }) => {
	const [answer, setAnswer] = useState("");
	const sendResponse = useResponse();

	return (
		<>
			<div>{prompt}</div>
			<input onChange={(ev) => setAnswer(ev.target.value)} />
			<button
				onClick={() =>
					sendResponse([
						{
							subject: subject || answer,
							relationship: relationship,
							object: object || answer,
							certainty: 100,
						},
					])
				}
			>
				Submit
			</button>
		</>
	);
};

const Result = ({ result }) => (
	<div>
		{result.subject} - {result.relationship} - {result.object} (
		{result.certainty}%)
	</div>
);

export default App;
